#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""rhove.py



Example usage

Criterion:

    -   Timeliness of Response
    -   Type of Response
    -   Seriousness of Complaints
"""

import argparse
import pandas as pd
import numpy as np

# Assign weights to the public responses ("Company public response"): -1, 0, or 1
# Good response = 1, Neutral response = 0, Poor response = -1
public_responses = {
    "Company has responded to the consumer and the CFPB and chooses not to provide a public response": -1,
    "Company believes it acted appropriately as authorized by contract or law": 0,
    "Company believes the complaint is the result of a misunderstanding": 0,
    "Company disputes the facts presented in the complaint": -1,
    "Company believes complaint caused principally by actions of third party outside the control or direction of the company": -1,
    "Company believes complaint is the result of an isolated error": -1,
    "Company can't verify or dispute the facts in the complaint": -1,
    "Company believes complaint represents an opportunity for improvement to better serve consumers": 1,
    "Company believes complaint relates to a discontinued policy or procedure": 0,
    "Company chooses not to provide a public response": 0,
}

# Assign weights to the private responses ("Company response to consumer"): -1, 0, or 1
# Good response = 1, Neutral response = 0, Poor response = -1
private_responses = {
    "Closed with explanation": 1,
    "Closed": 0,
    "Closed without relief": -1,
    "Closed with relief": 1,
    "In progress": 0,
    "Closed with monetary relief": 1,
    "Closed with non-monetary relief": 1,
    "Untimely response": -1,
}

def to_bool(value):
    """Convert "yes" and "no" to boolean values."""
    if value.lower() in ("yes", ):
        return True
    elif value.lower() in ("no", ):
        return False
    else:
        return np.nan

def evaluate_responses(series):
    """Produce a numerical value to represent how good a company's responses are."""
    weight = dict(**private_responses, **public_responses)
    return sum([weight.get(complaint, 0)>0 for complaint in series])

def company_summary(df):
    """Produce a company-level measure of response quality."""

    summary = df.groupby("Company").agg({
        "Complaint ID": ["count", ],
        "Timely response?": ["sum", ],
        "Company public response": [evaluate_responses, ]
    })

    # Rename the aggregated columns
    summary.columns = (
        "Complaints",
        "Timely Responses",
        "Positive Public Responses",
    )

    # Compute metrics by which to sort companies
    summary["Timely Response Ratio"] = \
        summary["Timely Responses"] / summary["Complaints"]

    summary["Positive Public Response Ratio"] = \
        summary["Positive Public Responses"] / summary["Complaints"]

    # Reorder the rows from best to worst
    summary.sort_values(
        [
            "Timely Response Ratio",
            "Positive Public Response Ratio",
            "Complaints",
        ],
        ascending=[
            False,
            False,
            True,
        ],
        inplace=True,
    )

    return summary

def get_data(path_to_file):

    with open(path_to_file) as input_file:

        # Load the data from the CSV file
        df = pd.read_csv(
            input_file,
            parse_dates=[
                "Date received",
                "Date sent to company",
            ],
            converters={
                "Consumer consent provided?": str,
                "Timely response?": to_bool,
            }
        )

        return df

def main(path_to_file):
    df = get_data(path_to_file)

    _company_summary = company_summary(df)
    _company_summary.sort_values(
        [
            "Timely Response Ratio",
            "Positive Public Response Ratio",
            "Complaints",
        ],
        ascending=[
            False,
            False,
            True,
        ],
        inplace=True,
    )
    return _company_summary

def cli():

    parser = argparse.ArgumentParser(
        prog="rhove.py",
        description="Rhove Complaint Analyzer",
        formatter_class=argparse.RawTextHelpFormatter,
        epilog="Example Usage:\n\tpython rhove.py -f Consumer_Complaints.csv -o Company_Summary.csv"
    )

    parser.add_argument(
        "-f", "--input-file",
        required=True,
        help="Input data file to read from"
    )
    parser.add_argument(
        "-o", "--output-file",
        required=True,
        help="Output data file to write to"
    )

    args = parser.parse_args()

    complaints = get_data(args.input_file)

    summary = company_summary(complaints)

    summary.to_csv(args.output_file)

if __name__ == "__main__":
    cli()
